class AddMemberTypeIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :member_type_id, :integer
  end
end
