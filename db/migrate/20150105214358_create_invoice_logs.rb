class CreateInvoiceLogs < ActiveRecord::Migration
  def change
    create_table :invoice_logs do |t|
      t.integer :user_id
      t.integer :invoice_id
      t.integer :status_id
      t.text :description
      
      t.timestamps
    end
  end
end
