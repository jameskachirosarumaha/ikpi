class AddColumnFinishToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :is_finished, :boolean, :default => false
    add_column :invoices, :finish_on, :datetime
    add_column :invoices, :finish_created_by_id, :integer
  end
end
