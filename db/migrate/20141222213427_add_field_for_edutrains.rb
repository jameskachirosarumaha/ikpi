class AddFieldForEdutrains < ActiveRecord::Migration
  def change
    add_column :edutrains, :is_publish, :boolean, :default => false
    add_column :edutrains, :is_finish, :boolean, :default => false 
    add_column :edutrains, :price, :decimal
  end
end
