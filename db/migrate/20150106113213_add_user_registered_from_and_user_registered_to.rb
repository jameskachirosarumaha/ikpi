class AddUserRegisteredFromAndUserRegisteredTo < ActiveRecord::Migration
  def change
    add_column :users, :registered_from, :datetime
    add_column :users, :registered_to, :datetime
  end
end
