class AddFieldForUserEdutrains < ActiveRecord::Migration
  def change
    add_column :user_edutrains, :is_finish, :boolean, :default => false
    add_column :user_edutrains, :is_paid, :boolean, :default => false
    add_column :user_edutrains, :is_agreed, :boolean, :default => false
    add_column :user_edutrains, :invoice_id, :integer
  end
end
