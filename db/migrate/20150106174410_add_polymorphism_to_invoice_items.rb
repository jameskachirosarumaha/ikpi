class AddPolymorphismToInvoiceItems < ActiveRecord::Migration
  def change
    add_column :invoice_items, :resource_id, :integer
    add_column :invoice_items, :resource_type, :string
  end
end
