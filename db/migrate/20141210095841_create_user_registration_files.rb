class CreateUserRegistrationFiles < ActiveRecord::Migration
  def change
    create_table :user_registration_files do |t|
      t.string :title
      t.text :description
      t.boolean :is_published, :default => true
      t.boolean :is_required, :default =>  true
      
      t.timestamps
    end
    
    add_column :user_files, :user_registration_file_id, :integer
  end
end
