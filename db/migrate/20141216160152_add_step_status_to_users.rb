class AddStepStatusToUsers < ActiveRecord::Migration
  def change
    add_column :users, :step_status, :integer, :default => 0
  end
end
