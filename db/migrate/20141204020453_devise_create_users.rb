class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      ## Database authenticatable
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.inet     :current_sign_in_ip
      t.inet     :last_sign_in_ip

      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at
      
      t.string :provider
      t.string :uid
      t.string :name
      t.string :nick_name
      t.string :first_name
      t.string :last_name
      t.string :gender
      t.string :domicile
      t.datetime :birthdate
      t.integer :member_type
      t.string   :birthplace
      t.string   :id_card_no
      t.string   :tax_card_no
      t.integer  :religion_id  
      t.string :oauth_token
      t.datetime :oauth_expires_at
      t.integer :correspondent_id
      t.integer :member_card_delivery_status
      t.integer :files_delivery_status
      t.boolean :confirmation_status, :default => false
      t.string :phone_no
      t.string :mobile_phone_no
      t.string :fax_no
      t.integer :member_status, :default => 0
      t.text :admin_note
      t.text :short_description
      t.text :description
      t.integer :total_pcu
      t.text :ca_address
      t.string :ca_rt
      t.string :ca_rw
      t.string :ca_subdistrict
      t.string :ca_district
      t.string :ca_province
      t.string :ca_city
      t.string :ca_postal_code
      t.attachment :avatar
      t.integer :status_stage, :default => 0
      t.boolean :is_approved, :default => false
      
      t.timestamps
    end

    add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
    # add_index :users, :confirmation_token,   unique: true
    # add_index :users, :unlock_token,         unique: true
    create_table(:user_educations) do |t|
      t.datetime :date_of_diploma
      t.integer :education_institution_stage_id
      t.string  :education_institution_name
      t.string  :education_institution_course
      t.string  :education_institution_city
      t.integer :user_id

      t.timestamps
    end
    create_table(:user_works) do |t|
      t.string :work_place_name
      t.string :work_place_position
      t.text :work_place_address
      t.string :work_place_postal_code
      t.string :work_place_fax_no
      t.string :work_place_phone_no
      t.string :work_place_phone_extension_no
      t.string :work_place_position
      t.string :work_place_division
      t.integer :work_place_job_level_id
      t.integer :work_place_field_of_interest_id
      t.integer :work_place_other_foi_id
      t.integer :work_place_type_id
      t.integer :work_place_business_category_id
      t.datetime :work_place_entry_date
      t.datetime :work_place_out_date
      t.text :work_place_short_description
      t.boolean :is_current, :default => false
      t.integer :user_id

      t.timestamps
    end
    create_table(:user_certifications) do |t|
      t.string :certification_date_of_graduation
      t.string :certification_name
      t.string :certification_no
      t.string :certification_issuer
      t.text :certification_description
      t.attachment :file
      t.integer :user_id

      t.timestamps
    end
    create_table(:user_pcus) do |t|
      t.integer :activity_subscription_type_id
      t.integer :activity_type_id
      t.datetime :activity_start_date
      t.datetime :activity_end_date
      t.text :activity_description 
      t.string :activity_title 
      t.text :proposal_pcu
      t.text :activity_organizer
      t.integer :user_id
      t.integer :pcu_value
      t.attachment :file
      t.integer :status_id
      t.boolean :is_claimed, :default => false
      t.integer :user_edutrain_id
      
      t.timestamps
    end
    create_table(:user_edutrains) do |t| 
      t.integer :user_id 
      t.integer :edutrain_id
      t.integer :status_id
      t.boolean :is_confirm, :default => false
      t.attachment :file
      t.text :description
      
      t.timestamps
    end
    create_table(:edutrains) do |t|
      t.string :name 
      t.string :edutrain_class_name
      t.string :trainer_name
      t.datetime :start_time
      t.datetime :end_time
      t.datetime :edutrain_start_date
      t.datetime :edutrain_end_date
      t.text :edutrain_place
      t.string :pcu_value 
      t.integer :edutrain_type_id
      t.integer :edutrain_subscription_type_id
      t.text :rules_and_regulation
      t.text :short_description
      t.text :description
      t.attachment :image
      
      t.timestamps
    end
    create_table(:user_files) do |t|
      t.string :name 
      t.integer :user_file_type_id
      t.boolean :is_required, :default => false
      t.boolean :is_avail
      t.text :short_description
      t.text :description
      t.attachment :file
      
      t.timestamps
    end
    create_table(:addresses) do |t|
      t.text :location
      t.string :rt
      t.string :rw 
      t.integer :subdistrict_id
      t.integer :district_id
      t.integer :city_id 
      t.integer :province_id
      t.string :postal_code
      t.integer :user_id
      t.boolean :is_default, :default => false
      
      t.timestamps
    end 
    
    create_table :provinces do |t| 
      t.string :name 
      t.text :description
      t.string :postal_code
      
      t.timestamps
    end 
    
    create_table :cities do |t| 
      t.string :name
      t.integer :province_id 
      t.text :description
      t.string :postal_code
      
      t.timestamps
    end
    
    create_table :districts do |t| 
      t.integer :city_id
      t.string :name
      t.text :description
      
      t.timestamps
    end
    
    create_table :subdistricts do |t| 
      t.integer :district_id
      t.string :name
      t.text :description
      t.string :postal_code
      
      t.timestamps
    end 
    
  end
end
