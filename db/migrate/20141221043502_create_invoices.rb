class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :user_id
      t.string  :invoice_note_id
      t.string  :invoice_title
      t.integer :total_amount
      t.decimal :total_price
      t.string  :currency
      t.text    :invoice_description
      t.text    :notes
      t.integer :status_id
      t.integer :invoice_type_id
      t.boolean :is_confirmed, :default => false
      t.boolean :is_invoice_items, :default => false
      
      t.timestamps 
    end
    create_table :invoice_items do |t|
      t.integer :invoice_id
      t.integer :amount
      t.decimal :price
      t.decimal :line_price
      t.string  :title
      t.text    :description
      
      t.timestamps 
    end
    create_table :invoice_confirmations do |t|
      t.integer   :invoice_id
      t.datetime  :paid_on
      t.string    :title
      t.text      :description
      t.integer   :payment_method_id
      t.attachment :payment_copy
      t.string    :paid_by
      t.string    :confirmation_number
      
      t.timestamps 
    end
    
  end
end
