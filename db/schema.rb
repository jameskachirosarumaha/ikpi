# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150106174410) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: true do |t|
    t.text     "location"
    t.string   "rt"
    t.string   "rw"
    t.integer  "subdistrict_id"
    t.integer  "district_id"
    t.integer  "city_id"
    t.integer  "province_id"
    t.string   "postal_code"
    t.integer  "user_id"
    t.boolean  "is_default",     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cities", force: true do |t|
    t.string   "name"
    t.integer  "province_id"
    t.text     "description"
    t.string   "postal_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "districts", force: true do |t|
    t.integer  "city_id"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "edutrains", force: true do |t|
    t.string   "name"
    t.string   "edutrain_class_name"
    t.string   "trainer_name"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "edutrain_start_date"
    t.datetime "edutrain_end_date"
    t.text     "edutrain_place"
    t.string   "pcu_value"
    t.integer  "edutrain_type_id"
    t.integer  "edutrain_subscription_type_id"
    t.text     "rules_and_regulation"
    t.text     "short_description"
    t.text     "description"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_publish",                    default: false
    t.boolean  "is_finish",                     default: false
    t.decimal  "price"
  end

  create_table "invoice_confirmations", force: true do |t|
    t.integer  "invoice_id"
    t.datetime "paid_on"
    t.string   "title"
    t.text     "description"
    t.integer  "payment_method_id"
    t.string   "payment_copy_file_name"
    t.string   "payment_copy_content_type"
    t.integer  "payment_copy_file_size"
    t.datetime "payment_copy_updated_at"
    t.string   "paid_by"
    t.string   "confirmation_number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoice_items", force: true do |t|
    t.integer  "invoice_id"
    t.integer  "amount"
    t.decimal  "price"
    t.decimal  "line_price"
    t.string   "title"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "resource_id"
    t.string   "resource_type"
  end

  create_table "invoice_logs", force: true do |t|
    t.integer  "user_id"
    t.integer  "invoice_id"
    t.integer  "status_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoices", force: true do |t|
    t.integer  "user_id"
    t.string   "invoice_note_id"
    t.string   "invoice_title"
    t.integer  "total_amount"
    t.decimal  "total_price"
    t.string   "currency"
    t.text     "invoice_description"
    t.text     "notes"
    t.integer  "status_id"
    t.integer  "invoice_type_id"
    t.boolean  "is_confirmed",         default: false
    t.boolean  "is_invoice_items",     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_finished",          default: false
    t.datetime "finish_on"
    t.integer  "finish_created_by_id"
  end

  create_table "provinces", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "postal_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "subdistricts", force: true do |t|
    t.integer  "district_id"
    t.string   "name"
    t.text     "description"
    t.string   "postal_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_certifications", force: true do |t|
    t.string   "certification_date_of_graduation"
    t.string   "certification_name"
    t.string   "certification_no"
    t.string   "certification_issuer"
    t.text     "certification_description"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_educations", force: true do |t|
    t.datetime "date_of_diploma"
    t.integer  "education_institution_stage_id"
    t.string   "education_institution_name"
    t.string   "education_institution_course"
    t.string   "education_institution_city"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_edutrains", force: true do |t|
    t.integer  "user_id"
    t.integer  "edutrain_id"
    t.integer  "status_id"
    t.boolean  "is_confirm",        default: false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_finish",         default: false
    t.boolean  "is_paid",           default: false
    t.boolean  "is_agreed",         default: false
    t.integer  "invoice_id"
  end

  create_table "user_files", force: true do |t|
    t.string   "name"
    t.integer  "user_file_type_id"
    t.boolean  "is_required",               default: false
    t.boolean  "is_avail"
    t.text     "short_description"
    t.text     "description"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_registration_file_id"
    t.integer  "user_id"
  end

  create_table "user_pcus", force: true do |t|
    t.integer  "activity_subscription_type_id"
    t.integer  "activity_type_id"
    t.datetime "activity_start_date"
    t.datetime "activity_end_date"
    t.text     "activity_description"
    t.string   "activity_title"
    t.text     "proposal_pcu"
    t.text     "activity_organizer"
    t.integer  "user_id"
    t.integer  "pcu_value"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "status_id"
    t.boolean  "is_claimed",                    default: false
    t.integer  "user_edutrain_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_registration_files", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "is_published", default: true
    t.boolean  "is_required",  default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_works", force: true do |t|
    t.string   "work_place_name"
    t.string   "work_place_position"
    t.text     "work_place_address"
    t.string   "work_place_postal_code"
    t.string   "work_place_fax_no"
    t.string   "work_place_phone_no"
    t.string   "work_place_phone_extension_no"
    t.string   "work_place_division"
    t.integer  "work_place_job_level_id"
    t.integer  "work_place_field_of_interest_id"
    t.integer  "work_place_other_foi_id"
    t.integer  "work_place_type_id"
    t.integer  "work_place_business_category_id"
    t.datetime "work_place_entry_date"
    t.datetime "work_place_out_date"
    t.text     "work_place_short_description"
    t.boolean  "is_current",                      default: false
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                       default: "",    null: false
    t.string   "encrypted_password",          default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",               default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "nick_name"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "gender"
    t.string   "domicile"
    t.datetime "birthdate"
    t.integer  "member_type"
    t.string   "birthplace"
    t.string   "id_card_no"
    t.string   "tax_card_no"
    t.integer  "religion_id"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.integer  "correspondent_id"
    t.integer  "member_card_delivery_status"
    t.integer  "files_delivery_status"
    t.boolean  "confirmation_status",         default: false
    t.string   "phone_no"
    t.string   "mobile_phone_no"
    t.string   "fax_no"
    t.integer  "member_status",               default: 0
    t.text     "admin_note"
    t.text     "short_description"
    t.text     "description"
    t.integer  "total_pcu"
    t.text     "ca_address"
    t.string   "ca_rt"
    t.string   "ca_rw"
    t.string   "ca_subdistrict"
    t.string   "ca_district"
    t.string   "ca_province"
    t.string   "ca_city"
    t.string   "ca_postal_code"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "status_stage",                default: 0
    t.boolean  "is_approved",                 default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "step_status",                 default: 0
    t.integer  "member_type_id"
    t.datetime "registered_from"
    t.datetime "registered_to"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
