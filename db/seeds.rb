# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#f
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin = User.with_role(:admin).take
if admin.blank?
  admin = User.new
  admin.password = "Ikpi@dm1n"
  admin.password_confirmation = "Ikpi@dm1n"
  admin.email = "hello@admin.com"
  admin.save
  admin.confirm!
  admin.add_role :admin
end
