class InvoicesController < ApplicationController 
  before_filter :authenticate_user!
  before_filter :set_invoice, :except => [:index, :new, :create]
  
  def index
    @invoices = current_user.invoices
  end
  
  def show
  end
  
  def confirmation 
  end
  
  def create_confirmation
    if @invoice.invoice_confirmation.blank?
      invoice_confirmation = InvoiceConfirmation.new(invoice_confirmation_params)
      if invoice_confirmation.save
        @invoice.update({"is_confirmed" => true, "status_id" => 1})
        flash["notice"] = "Konfirmasi Telah Kami Terima Dan Sedang Di Proses"
      else
        flash["notice"] = "Terjadi Kesalahan Pada Saat Menginput Data. Silahkan Coba Kembali."
        redirect_to confirmation_invoice_path(@invoice) and return
      end
    else
      invoice_confirmation = @invoice.invoice_confirmation
      if invoice_confirmation.update(invoice_confirmation_params)
        @invoice.update({"is_confirmed" => true, "status_id" => 1})
        flash["notice"] = "Konfirmasi Telah Kami Terima Dan Sedang Di Proses"
      else
        flash["notice"] = "Terjadi Kesalahan Pada Saat Menginput Data. Silahkan Coba Kembali."
        redirect_to confirmation_invoice_path(@invoice) and return
      end
    end
    redirect_to invoices_path
  end
  
  private
    def set_invoice
      @invoice = Invoice.find_by id: params[:id]
    end
    
    def invoice_confirmation_params
      params.require(:invoice_confirmation).permit(:paid_on, :paid_by, :invoice_id, :description, :title, :payment_method_id, :payment_copy)
    end
    
end
