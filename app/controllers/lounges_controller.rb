class LoungesController < ApplicationController 
  before_filter :authenticate_user!
  
  def index
    @edutrains = Edutrain.where("is_publish IS TRUE").order("created_at DESC")
    @user_edutrains = []
    current_user.user_edutrains.each do |user_edu|
       @user_edutrains << user_edu.edutrain.id
    end
  end
  
  def profile
    @user_works = current_user.user_works.order("created_at DESC")
    @user_certifications = current_user.user_certifications.order("created_at DESC")
    @user_educations = current_user.user_educations.order("created_at DESC")
    render :layout => "blank"
  end
  
  def change_password
  end
  
  def update_password 
    
    if params["new_password"].present? && params["new_password_confirmation"].present?
    
      if current_user.valid_password?(params["current_password"])
        user = current_user
        if current_user.update({"password" => params["new_password"], "password_confirmation" => params["new_password_confirmation"]})
          sign_in( user, :bypass => true ) 
          flash["notice"] = "Password Anda Berhasil Diubah"
        else
          flash["notice"] = "Password Baru Anda Tidak Valid"
        end
      else
        flash["notice"] = "Password Anda Tidak Valid"
      end
    else
      flash["notice"] = "Password Baru Anda Tidak Boleh Kosing"
    end
    redirect_to change_password_lounges_path
  end
  
  private
end
