class MemberController < LoungesController
  layout "blank"
  
  def new 
    @user_registration_files = UserRegistrationFile.where("is_published IS TRUE").order("created_at DESC")
    @user_educations = current_user.user_educations.order("created_at DESC")
    @user_works = current_user.user_works.order("created_at DESC")
    @user_certifications = current_user.user_certifications.order("created_at DESC")
    @user_files = current_user.user_files.collect(&:user_registration_file_id)
    @reg_invoice = current_user.invoices.where("invoice_type_id = ?", 0).order("created_at DESC").take
  end
  
  def form_type
    if current_user.member_status == 0
      if current_user.update({"member_type_id" => params["form_id"]})
        redirect_to new_member_path and return
      end 
    end
    if current_user.member_status == 1
      if current_user.update({"member_type_id" => 3})
      end
    end
    redirect_to registration_member_index_path and return
  end
  
  def registration
    render :layout => "application"
  end
  
  def update 
    if current_user.update(user_params)
      current_user.update({step_status: 1}) if current_user.step_status != 5
      return render :json => {"success" => true, "user" => current_user}
    end
    return render :json => {"success" => false}
  end
  
  def validate 
    current_user.update({"status_stage" => 1})
    flash[:notice] = "Kami Sedang Memproses Aplikasi Anda"
    redirect_to lounges_path
  end
  
  def download_user_file
    user_file  = UserFile.find_by id: params["user_file_id"]
    if user_file.present?
      send_file user_file.file.path, :disposition => 'attachment'
    end
  end
  
  def update_my_files
    new_file = []
    if params["user_registration_file"].present?
      params["user_registration_file"].each do |k, v|
        user_registration_file = UserRegistrationFile.find_by id: k
        if user_registration_file.present?
          user_file = UserFile.find_by user_registration_file_id: k
          if user_file.blank?
            user_file = UserFile.new
            user_file.user_registration_file_id = user_registration_file.id
            user_file.user_id = current_user.id
            user_file.file = v
            if user_file.save
              new_file << {"id" => user_file.id, "title" => user_registration_file.title, "is_required" => user_registration_file.is_required, "is_avail" => user_file.is_avail, "short_description" => user_file.short_description, "description" => user_file.description, "filename" => user_file.file.original_filename, "file_url" => user_file.file.url, "user_registration_file_id" => user_file.user_registration_file_id}
            end
          else
            user_file.update({"file" => v})
            new_file << {"id" => user_file.id, "title" => user_registration_file.title, "is_required" => user_registration_file.is_required, "is_avail" => user_file.is_avail, "short_description" => user_file.short_description, "description" => user_file.description, "filename" => user_file.file.original_filename, "file_url" => user_file.file.url, "user_registration_file_id" => user_file.user_registration_file_id}
          end
        end 
      end
      
      if new_file.present?
        if current_user.step_status != 5
          current_user.update({"step_status" => 5})
        end
      end
    end
    return render :json => {"files" => new_file, "success" => true, "user" => current_user}
  end
  
  def update_education_history
    user_education = UserEducation.new(user_education_params)
    user_education.user_id = current_user.id 
    if user_education.save 
    
      if current_user.step_status != 2 and current_user.step_status != 5
        current_user.update({"step_status" => 2})
      end
      user_education = {
        "id" => user_education.id,
        "education_institution_stage" => Preference::EDU_STAGE[user_education.education_institution_stage_id],
        "date_of_diploma" => user_education.date_of_diploma.present? ? user_education.date_of_diploma.strftime("%d-%m-%Y") : "-",
        "education_institution_name" => user_education.education_institution_name,
        "education_institution_course" => user_education.education_institution_course,
        "education_institution_city" => user_education.education_institution_city
      
      }
      return render :json => {"success" => true, "user" => current_user, "user_education" => user_education}
    end
    return render :json => {"success" => false}
  end 
  
  def update_work_history
    user_work = current_user.user_works.where("user_works.is_current IS TRUE").take 
    if (params["work"]["is_current"] == "false") || user_work.blank?
      user_work_else = UserWork.new(user_work_params)
      user_work_else.user_id = current_user.id 
      if user_work_else.save 
        if current_user.step_status != 3 and current_user.step_status != 5
          current_user.update({"step_status" => 3}) if user_work.present?
        end
        
        user_work_else = {
          "work_place_name" => user_work_else.work_place_name,
          "work_place_position" => user_work_else.work_place_position,
          "work_place_division" => user_work_else.work_place_division,
          "work_place_entry_date" => user_work_else.work_place_entry_date.present? ? user_work_else.work_place_entry_date.strftime("%d-%m-%Y") : "-",
          "work_place_out_date" => user_work_else.work_place_out_date.present? ? user_work_else.work_place_out_date.strftime("%d-%m-%Y") : "-",
          "work_place_short_description" => user_work_else.work_place_short_description,
          "id" => user_work_else.id
        }
        return render :json => {"success" => true, "user" => current_user, "user_work" => user_work_else}
      end
    else
      user_work_length = current_user.user_works.count("user_works.is_current IS NOT TRUE")
      if user_work.update(user_work_params)
      
        if current_user.step_status != 3 and current_user.step_status != 5
          current_user.update({"step_status" => 3}) if user_work_length > 0
        end
        
        return render :json => {"success" => true, "user" => current_user, "user_work" => user_work}                                                                                                                 
      end
    end
    
    return render :json => {"success" => false}
  end 
  
  def update_my_certification
    user_certification = UserCertification.new(user_certification_params)
    user_certification.user_id = current_user.id 
    if user_certification.save 
      if current_user.step_status != 4 and current_user.step_status != 5
        current_user.update({"step_status" => 4})
      end
      
      user_certification = {
        "certification_date_of_graduation" => user_certification.certification_date_of_graduation,
        "certification_name" => user_certification.certification_name,
        "certification_no" => user_certification.certification_no,
        "certification_issuer" => user_certification.certification_issuer,
        "id" => user_certification.id
      }
      
      return render :json => {"success" => true, "user" => current_user, "user_certification" => user_certification}
    end
    return render :json => {"success" => false}
  end
  
  def delete_education_history
    eduhis = current_user.user_educations.find_by id: params["user_education_id"]
    if eduhis.destroy
      return render :json => {"success" => true}
    end
    return render :json => {"success" => false}
  end
  
  def delete_work_history
    user_work = current_user.user_works.find_by id: params["user_work_id"]
    if user_work.present?
      if user_work.destroy
        return render :json => {"success" => true}
      end
    end
    return render :json => {"success" => false}
  end
  
  def delete_my_certification
    usercert = current_user.user_certifications.find_by id: params["user_certification_id"]
    if usercert.destroy
      return render :json => {"success" => true}
    end
    return render :json => {"success" => false}
  end
  
  def delete_my_file
    userfile = current_user.user_files.find_by id: params["user_file_id"]
    if userfile.destroy
      return render :json => {"success" => true}
    end
    return render :json => {"success" => false}
  end


private

  def user_params
    params.require(:user).permit(:name, :nick_name, :first_name, :last_name, :gender, :domicile, :birthdate, :member_type, :birthplace, :id_card_no,
:tax_card_no, :religion_id, :correspondent_id, :member_card_delivery_status, :files_delivery_status,
:confirmation_status, :phone_no, :mobile_phone_no, :fax_no, :member_status, :admin_note, :short_description,
:description, :total_pcu, :ca_address, :ca_rt, :ca_rw, :ca_subdistrict, :ca_district, :ca_province, 
:ca_city, :ca_postal_code, :avatar, :status_stage, :is_approved, :email, :step_status)
  end  
        
  def user_education_params
    params.require(:user_education).permit(:education_institution_city, 
    :education_institution_city, 
    :education_institution_course, 
    :education_institution_name, 
    :education_institution_stage_id, 
    :date_of_diploma)
  end
  
  def user_certification_params
    params.require(:user_certification).permit( :certification_date_of_graduation, 
    :certification_name, 
    :certification_no, 
    :certification_issuer, 
    :certification_description )
  end
  
  def user_work_params
    params.require(:work).permit( :work_place_name, 
    :work_place_position, 
    :work_place_address, 
    :work_place_postal_code, :work_place_fax_no, 
    :work_place_phone_no, 
    :work_place_phone_extension_no, 
    :work_place_job_level_id, 
    :work_place_division, 
    :work_place_field_of_interest_id, 
    :work_place_other_foi_id, 
    :work_place_type_id, 
    :work_place_business_category_id, 
    :work_place_entry_date, 
    :work_place_out_date, 
    :work_place_short_description, 
    :is_current, 
    :user_id)
  end
  
  def user_file_params
    params.require(:user_file).permit(:name, :user_file_type_id, :is_required, :is_avail, 
    :short_description, :description, :file)
  end

end
