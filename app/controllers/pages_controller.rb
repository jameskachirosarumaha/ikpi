class PagesController < ApplicationController 
  def landing 
    redirect_to lounges_path if current_user
    @edutrains = Edutrain.where("is_publish IS TRUE").order("created_at DESC")
  end
  
  private
end
