class Admin::DashboardsController < ApplicationController
  layout "admin"
  
  def index
    @recently_created_users = User.joins(:roles).where("roles.name != 'admin'").where("users.created_at >= ?", Time.zone.now.beginning_of_day)
    @validate_users = User.joins(:roles).where("roles.name != 'admin'").where("users.status_stage = ?", 1)
    @confirmable_invoices = Invoice.where("status_id = ?", 1).order("created_at DESC")
  end
  
end 
