class Admin::UserRegistrationFormsController < Admin::DashboardsController 
  before_filter :set_user_registration_form , :except => [:index]
  
  def index
    @user_regform_validations = User.joins(:roles).where("roles.name != 'admin'")
  end
  
  def show
    @user_registration_files = UserRegistrationFile.where("is_published IS TRUE").order("created_at DESC")
    @user_educations = @user.user_educations.order("created_at DESC")
    @user_works = @user.user_works.order("created_at DESC")
    @user_certifications = @user.user_certifications.order("created_at DESC")
    @user_files = @user.user_files.collect(&:user_registration_file_id)
    @reg_invoice = @user.invoices.where("invoice_type_id = ?", 0).order("created_at DESC").take
    render :layout => "blank"
  end
  
  def update 
    #perlu ada jagaan disaat pembuatan invoice 
    if params["accept_user_validation"].to_s == "true" 
      @user.update({"status_stage" => 2})
      
      if @user.invoices.where("invoice_type_id = ?", 0).length == 0
        invoice = Invoice.new({"invoice_title" => "Pembayaran Administrasi Keanggotaan IKPI", "invoice_type_id" => 0, "status_id" => 0, "total_amount" => 1, "total_price" => 1000000, "user_id" => @user.id, "invoice_note_id" => "INV-"+@user.generate_invoice_no.to_s})
        if invoice.save
          flash["notice"] = "Data Telah Disimpan dan Diproses. User Sudah Dapat Memiliki Tagihan Untuk Biaya Administrasi Keanggotaan"
        end
      end
      
    else
      @user.update({"status_stage" => 0, "admin_note" => params["user"]["admin_note"] })
      flash["notice"] = "Data Telah Disimpan dan Diproses"
    end
    
    redirect_to admin_user_registration_form_path(@user)
  end
  
  def cancel_reg_bills
    @user.update({"status_stage" => 1})
    invoices = @user.invoices.where("invoice_type_id = ?", 0)
    if invoices.present?
      invoices.destroy_all
    end
    flash["notice"] = "Tagihan Telah Dibatalkan"
    redirect_to admin_user_registration_form_path(@user)
  end
  
  private
  
    def set_user_registration_form
      @user = User.find_by id: params[:id]
    end
end
