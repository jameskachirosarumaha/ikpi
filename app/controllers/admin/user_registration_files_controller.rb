class Admin::UserRegistrationFilesController < Admin::DashboardsController
  before_filter :set_user_registration_file, :except => [:index, :create, :new]
  
  load_and_authorize_resource
  
  def index
    @user_registration_files = UserRegistrationFile.all.order("created_at DESC")
  end
  
  def new 
    @user_registration_file = UserRegistrationFile.new
  end
  
  def show
  end
  
  def create 
    @user_registration_file = UserRegistrationFile.new(user_registration_file_params)
    
    if @user_registration_file.save
      flash[:notice] = "Berhasil Membuat Berkas"
      redirect_to admin_user_registration_file_path(@user_registration_file) and return
    end
    render :action => :new
  end
  
  def edit 
  end
  
  def update 
    #user_registration_file_params.is_published = params["user_registration_file"]["is_published"].blank? ? false : params["user_registration_file"]["is_published"]
    #user_registration_file_params.is_required = params["user_registration_file"]["is_required"].blank? ? false : params["user_registration_file"]["is_required"]
    if @user_registration_file.update(user_registration_file_params)
      flash[:notice] = "Berhasil Menyunting Berkas"
      redirect_to admin_user_registration_file_path(@user_registration_file) and return
      
    end
    render :action => :edit
  end
  
  def destroy
    if @user_registration_file.destroy
      flash[:notice] = "Berhasil Menghapus Berkas"
      redirect_to admin_user_registration_files_path and return
    end
    flash[:notice] = "Tidak Berhasil Menghapus Berkas - #{@user_registration_file.title}"
    redirect_to admin_user_registration_files_path and return    
  end
  
  private
  
    def set_user_registration_file
      @user_registration_file = UserRegistrationFile.find_by id: params[:id]
    end
    
    def user_registration_file_params
      params.require(:user_registration_file).permit(:title, :description, :is_published, :is_required)
    end
  
end
