class Admin::UsersController < Admin::DashboardsController

  load_and_authorize_resource
  
  def index
    @users =  User.joins(:roles).where("roles.name != 'admin'").order("created_at DESC")
  end
  
  def set_registration_date
    @user = User.find_by id: params["id"]
  end
  
  def create_registration_date
    @user = User.find_by id: params["id"]
    if @user.present?
      if params["start_date"].present? && params["end_date"].present? 
        if @user.update({"registered_from" => params["start_date"], "registered_to" => params["end_date"]})
          flash["notice"] = "Data Berhasil Disimpan"
          redirect_to admin_users_path and return
        else
          flash["notice"] = "Tidak Bisa Minyimpan Data Tanggal. Silahkan Coba Lagi" 
        end
      else
        flash["notice"] = "Tanggal Mulai Dan Berakhir Wajib Diisi." 
      end
    else
      flash["notice"] = "Tidak Bisa Mengatur Validasi Keanggotaan. Silahkan Coba Lagi"
    end
    redirect_to set_registration_date_admin_user_path(@user) and return
  end
  
end
