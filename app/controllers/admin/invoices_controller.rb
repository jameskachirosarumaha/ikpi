class Admin::InvoicesController < Admin::DashboardsController 
  before_filter :set_invoice , :except => [:index, :new, :create]
  
  def index
    @invoices = Invoice.all
  end
  
  def show
  end
  
  def confirmation
  end
  
  def create_confirmation  
    if params["invoice"]["is_finished"].to_s == "true"
      @invoice.update({"is_finished" => true, "finish_on" => Time.now, "finish_created_by_id" => current_user.id, "status_id" => 2})
      
      flash["notice"] = "Pembayaran #{@invoice.invoice_note_id} Telah Terkonfirmasi"
      
      if @invoice.invoice_type_id == 0
        @invoice.user.update({"status_stage" => 3, "member_status" => 1, :is_approved => true})
        redirect_to set_registration_date_admin_user_path(@invoice.user_id) and return
      end
    else
      flash["notice"] = "Data Konfirmasi Anda Telah Disimpan"
    end
    
    redirect_to admin_invoices_path
  end
  
  def cancel_confirmation
    if params["invoice_note"].present?
      if @invoice.update({"status_id" => 0, "is_confirmed" => false})
        invoice_log = InvoiceLog.new
        invoice_log.invoice_id = @invoice.id
        invoice_log.user_id = @invoice.user_id
        invoice_log.status_id = 0
        invoice_log.description = params["invoice_note"]
        
        if invoice_log.save
          flash["notice"] = "Konfirmasi Invoice #{@invoice.invoice_note_id} Telah Dibatalkan"
        else
          flash["notice"] = "Invoice Log Tidak Berhasil Dibuat, Konfirmasi Invoice #{@invoice.invoice_note_id} Telah Dibatalkan"
          redirect_to confirmation_admin_invoice_path(@invoice) and return
        end
      else
        flash["notice"] = "Invoice Tidak Bisa Di Batalkan. Silahkan Coba Kembali"
        redirect_to confirmation_admin_invoice_path(@invoice) and return
      end
    else
      flash["notice"] = "Silahkan Meninggalkan Pesan Untuk Melakukan Pembatalan"
      redirect_to confirmation_admin_invoice_path(@invoice) and return
    end
    redirect_to admin_invoices_path
  end
  
  def destroy
    if @invoice.destroy
      flash["notice"] = "Invoice berhasil Dihapus"
    else
      flash["notice"] = "Invoice Tidak berhasil Dihapus"
    end
    redirect_to admin_invoices_path
  end
 
  
  private
  
    def set_invoice
      @invoice = Invoice.find_by id: params[:id]
    end
end
