class Admin::EdutrainsController < Admin::DashboardsController
  before_filter :set_edutrain, :except => ["index", "new", "create"]
  
  def index
    @edutrains = Edutrain.order("created_at DESC")
  end
  
  def new
    @edutrain = Edutrain.new
  end
  
  def show
  end
  
  def edit
  end

  def create 
    @edutrain = Edutrain.new(edutrain_params)
    if @edutrain.save 
      flash["notice"] = "Data Berhasil Disimpan"
      redirect_to admin_edutrain_path(@edutrain) and return
    else
      flash["notice"] = "Terjadi Kesalahan Ketika Penyimpan Data."
      render :action => :new and return
    end
  end 
  
  def update 
    if @edutrain.update(edutrain_params)
      flash["notice"] = "Data Berhasil Diperbaharui"
      redirect_to admin_edutrain_path(@edutrain) and return
    else
      flash["notice"] = "Terjadi Kesalahan Ketika Penyimpan Data."
      render :edit => :new and return
    end
  end
  
  def destroy 
    if @edutrain.destroy
      flash["notice"] = "Data Berhasil Dihapus"
    else
      flash["notice"] = "Data Tidak Berhasil Dihapus"
    end
    
    redirect_to admin_edutrains_path
    
  end 
  
  private
    def set_edutrain
      @edutrain = Edutrain.find_by id: params["id"]
    end
  
    def edutrain_params
      params.require(:edutrain).permit(:is_publish, :price, :is_finish, :name, :edutrain_class_name, 
      :trainer_name, :start_time, :end_time, :edutrain_start_date, 
      :edutrain_end_date, :edutrain_place, :pcu_value, :edutrain_type_id,
      :edutrain_subscription_type_id, :rules_and_regulation, 
      :short_description, :description, :image_file_name, :image_content_type,
      :image_file_size, :image_updated_at)
    end
end
