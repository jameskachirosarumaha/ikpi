class ConfirmationsController < Devise::ConfirmationsController
  # Remove the first skip_before_filter (:require_no_authentication) if you
  # don't want to enable logged users to access the confirmation page.
  skip_before_filter :require_no_authentication
  skip_before_filter :authenticate_user!

  # PUT /resource/confirmation
  def update 
  end

  # GET /resource/confirmation?confirmation_token=abcdef
  def show 
  
    original_token = params[:confirmation_token]
    confirmation_token = Devise.token_generator.digest(User, :confirmation_token, original_token)
    @user = User.find_or_initialize_with_error_by(:confirmation_token, confirmation_token)
    
    if @user.errors.empty?  
      @user.confirm!
      
      generated_password = resource.generate_password
      if @user.update({"password" => generated_password, :password_confirmation => generated_password})
        @user.unhashed_password = generated_password      
        Mailer.letter_of_acceptance(@user, generated_password).deliver
        @user.add_role :member
        sign_in( @user, :bypass => true ) 
      end 
    else
      redirect_to invalid_confirmation_path(original_token)
    end 
    
  end 
  
  def invalid_confirmation
  end
  
end
