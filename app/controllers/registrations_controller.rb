class RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters
   

  def create
    build_resource user_params 
    generated_password = resource.generate_password
    resource.password = generated_password
    resource.password_confirmation = generated_password   
    @user =  resource
    if  verify_recaptcha(:model => @user, :message => "Salah Captcha") && @user.save   
      flash["notice"] = "Kami telah mengirimkan sebuah pesan melalui email yang anda daftarkan. Silahkan mengkonfirmasi untuk melanjutkan pendaftaran keanggotan anda di IKPI Lounge."
      return redirect_to root_path
    else 
      render :action => :new and return
    end 
    redirect_to root_url
  end
  
  def ask_to_confirm
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up).push(:name, :email, :mobile_phone_no, :id_card_no)
  end
  
  private
   
  def user_params
    params.require(:user).permit(:name, :email, :mobile_phone_no, :id_card_no, :password, :password_confirmation)
  end
end
