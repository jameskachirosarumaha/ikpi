class SessionsController < Devise::SessionsController

  def new 
    self.resource = resource_class.new(sign_in_params)
    clean_up_passwords(resource)
  end

  def create
    user = User.find_by_email(params["user"]["email"])

    if user.present?
      if user.valid_password?(params["user"]["password"])
        sign_in( user, :bypass => true ) 
        
        if user.has_role? :admin 
          return redirect_to admin_dashboards_path
        else
          return redirect_to lounges_path
        end
      else
        flash[:notice] = "Email Atau Password Anda Tidak Valid."
      end
    end
    redirect_to root_url
  end

  def register
    user = User.new(user_params)
    generated_password = user.generate_password
    user.password = generated_password
    user.password_confirmation = generated_password 
    
    if user.save
      user.send_confirmation_instructions 
      return redirect_to root_path
    end
    raise user.errors.inspect
    redirect_to root_url
  end

  def forgot
    user = User.find_by_email(params["user"]["email"])
    if user.present?
      user.password = password = Devise.friendly_token.first(8)
      user.password_confirmation = password
      user.save

      Mailer.letter_format_of_change_password(user, password).deliver
    end
    redirect_to root_url
  end 
  
  def facebook
    user = User.from_omniauth(env["omniauth.auth"])

    if user.persisted? 
      sign_in( user, :bypass => true )  
      redirect_to root_url
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end
  
  def is_login
    return render :json => {"val" => current_user.present? ? true : false}
  end

 

  def failure
    raise params.inspect
  end

  def destroy
    if sign_out(current_user)
    	redirect_to root_url
    end
  end
  
  private
   
    def user_params
      params.require(:user).permit(:name, :email, :mobile_phone_no, :id_card_no)
    end
end
