class EdutrainsController < ApplicationController 
  before_filter :authenticate_user!, :except => [:show]
  before_filter :set_edutrain , :except => [:index, :new, :create]
  
  def index  
    @edutrains = Edutrain.where("is_publish IS TRUE").order("created_at DESC")
    @user_edutrains = []
    current_user.user_edutrains.each do |user_edu|
       @user_edutrains << user_edu.edutrain.id
    end
  end
  
  def show
  end
  
  def register 
    if params["user_edutrain"]["is_agreed"].to_s == "true"
      user_edutrain = UserEdutrain.new
      user_edutrain.user_id = current_user.id
      user_edutrain.edutrain_id = @edutrain.id
      if user_edutrain.save
        invoice = Invoice.new({"invoice_title" => "Pembayaran PPL - #{@edutrain.name}", "invoice_type_id" => 1, "status_id" => 0, "total_amount" => 1, "total_price" => @edutrain.price, "user_id" => current_user.id, "invoice_note_id" => "INV-"+current_user.generate_invoice_no.to_s})
        if invoice.save
          user_edutrain.update({"invoice_id" => invoice.id})
          flash["notice"] = "Anda Telah DiDaftarkan Ke PPL - #{@edutrain.name} - . Silahkan Melakukan Pembayaran Dengan Kode - #{invoice.invoice_note_id}"
        else
          user_edutrain.destroy
          flash["notice"] = "Terjadi Kesalahan Dalam Pendaftaran PPL. Silahkan Coba Kembali"
        end
      end
    else
      flash["notice"] = "Anda Belum Setuju Dengan Prasyarat PPL."
    end
    redirect_to edutrain_path(@edutrain)
  end
  
  private
  
    def set_edutrain
      @edutrain = Edutrain.find_by id: params["id"]
    end
end
