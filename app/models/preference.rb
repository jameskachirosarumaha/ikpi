class Preference
  MEMBER_STATUS_STAGE = ["Data Pribadi", "Pendidikan", "Pekerjaan", "Sertifikasi", "Pembayaran", "Approval"]
  HEDU_STAGE = { "SMA Atau Sederajat" => 0, "DIII" => 1, "DIV" => 2, "PPA" => 3, "S1" => 4, "S2" => 5, "S3" => 6, "Professor" => 7 }
  EDU_STAGE  = ["SMA Atau Sederajat", "DIII", "DIV", "PPA", "S1", "S2", "S3", "Professor"]
  
  JOB_LEVEL = ["Entry Level", "Middle Management", "Senior Management", "Top Management"]
  HJOB_LEVEL = {"Entry Level" => 0, "Middle Management" => 1, "Senior Management" => 2, "Top Management" => 3}
  
  JOB_FOI = ["Lainnya"]
  HJOB_FOI = {"Lainnya" => 0}
  
  JOB_OTH_FOI = ["Lainnya"]
  HJOB_OTH_FOI = {"Lainnya" => 0}
  
    
  JOB_BUSINESS_CAT = ["Pemerintah", "Pendidikan", "Manufaktur", "Perbankan", "Auditting & Assurance", "Konstruksi", "Konsultan", "Properti", "Asuransi", "Keuangan", "Pajak", "Migas", "Perdagangan", "Agrobisnis", "Hotel", "IT & Telekomunikas", "Shipping", "Lainnya"]
  HJOB_BUSINESS_CAT = {"Pemerintah" => 0, "Pendidikan" => 1, "Manufaktur" => 2, "Perbankan" => 3, "Auditting & Assurance" => 4, "Konstruksi" => 5, "Konsultan" => 6, "Properti" => 7, "Asuransi" => 8, "Keuangan" => 9, "Pajak" => 10, "Migas" => 11, "Perdagangan" => 12, "Agrobisnis" => 13, "Hotel" => 14, "IT & Telekomunikasi" => 15, "Shipping" => 17, "Lainnya" => 18}
  
  WORKPLACE_TYPE = ["Listed Company", "BUMN", "BUMD", "Multinasional", "Small Medium Entreprise", "Non-SME", "Lainnya"]
  HWORKPLACE_TYPE = {"Lainnya" => 0, "BUMN" => 1, "BUMD" => 2, "Multinasional" => 3, "Small Medium Entreprise" => 4, "Non-SME" => 5, "Lainnya" => 6}
  
  MEMBER_STAGE_STATUS = ["Tahap Pengisian Formulir", "Tahap Menunggu Validasi Data", "Tahap Pembayaran", "Approved"]
  
  INVOICE_STATUS = ["Menunggu Konfirmasi Pembayaran", "Konfirmasi Pembayaran Sedang Diproses", "Pembayaran Telah Diterima"]
  
  HEDUTRAIN_TYPE = {"Offline" => 0, "Online" => 1 }
  EDUTRAIN_TYPE = ["Offline", "Online"]
  
  INVOICE_TYPE = ["Pendaftaran Administrasi Keanggotaan IKPI", "Pendaftaran PPL"]
  HINVOICE_TYPE = {"Pendaftaran Administrasi Keanggotaan IKPI" => 0, "Pendaftaran PPL" => 1}
end
