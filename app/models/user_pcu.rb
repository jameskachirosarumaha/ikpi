class UserPcu < ActiveRecord::Base 
  belongs_to :user 
  has_attached_file :file
  validate :correct_content_type, :message => ", Only PDF, EXCEL, WORD or TEXT files are allowed."
  validates_attachment_size :file, :less_than => 10.megabytes     

  def correct_content_type 
    acceptable_types = ["application/pdf","application/vnd.ms-excel",     
               "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
               "application/msword", 
               "application/vnd.openxmlformats-officedocument.wordprocessingml.document", 
               "text/plain"]
    acceptable_types.include? uploaded_file.content_type.chomp
  end
end
