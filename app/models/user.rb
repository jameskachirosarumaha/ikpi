class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  attr_accessor :unhashed_password

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :omniauthable
         
  has_many :user_certifications
  has_many :user_edutrains
  has_many :edutrains, :through => :user_edutrains
  has_many :user_educations
  has_many :user_files
  has_many :user_registration_files, :through => :user_files
  has_many :user_works
  has_many :user_pcus
  
  has_many :invoices
  has_attached_file :avatar, :styles => { :medium => "350x350>", :thumb => "100x100>", :large => "650x650" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  
  
  validates :id_card_no, presence: true
  validates :name, presence: true
  validates :mobile_phone_no, presence: true
  
  def edutrain_ids
    edutrains = self.edutrains.collect(&:id)
    edutrains
  end
  
  def only_if_unconfirmed
    pending_any_confirmation {yield}
  end
  
  def generate_password
    char_input = [('a'..'z'), ('A'..'Z'), (0..9)].map { |i| i.to_a }.flatten
    char_password = (0...8).map { char_input[rand(char_input.length)] }.join
    char_password
  end
  
  def generate_invoice_no
    char_input = [(0..9)].map { |i| i.to_a }.flatten
    char_password = (0...10).map { char_input[rand(char_input.length)] }.join
    char_password
  end
  
  def current_workplace
    user_workplace = self.user_works.blank? ? UserWork.new : self.user_works.where("user_works.is_current IS TRUE").take
    user_workplace
  end
end
