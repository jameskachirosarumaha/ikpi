class Mailer < ActionMailer::Base

	def letter_of_acceptance(user, password)
		@user = user
		@url = "http://localhost:3000"
		@password = password
		mail(to: @user.email, subject: 'Welcome')
	end

	def letter_format_of_change_password(user, password)
		@user = user
		@url = "http://localhost:3000"
		@password = password
		mail(to: @user.email, subject: 'Change Password')
	end

end
