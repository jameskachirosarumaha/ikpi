class InvoiceItem < ActiveRecord::Base 
  belongs_to :invoice
  belongs_to :resource, :polymorphic => true
end
