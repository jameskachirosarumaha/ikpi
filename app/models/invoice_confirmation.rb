class InvoiceConfirmation < ActiveRecord::Base 
  belongs_to :invoice
  has_attached_file :payment_copy
  validates_attachment :payment_copy, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif", "application/pdf","application/vnd.ms-excel",     
               "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
               "application/msword", 
               "application/vnd.openxmlformats-officedocument.wordprocessingml.document", 
               "text/plain"] }
end
