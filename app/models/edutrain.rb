class Edutrain < ActiveRecord::Base 
  has_many :user_edutrains 
  has_attached_file :image, :styles => { :medium => "350x350>", :thumb => "100x100>", :large => "650x650" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
