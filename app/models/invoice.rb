class Invoice < ActiveRecord::Base 
  belongs_to :user
  has_one :invoice_confirmation
  has_many :invoice_items
end
