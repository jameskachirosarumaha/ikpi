module ApplicationHelper
  def get_file(selection_id)
    file = UserFile.find_by user_registration_file_id: selection_id
    file
  end
  
  def get_user_edutrain(id)
    @user_edutrain = current_user.user_edutrains.find_by edutrain_id: id
    @user_edutrain
  end
  
  def user_edutrain_invoice(current_user, edutrain)
    user_edutrain = UserEdutrain.where("user_id = ? AND edutrain_id = ?", current_user.id, edutrain.id).order("created_at DESC").take
    invoice_code = "###"
    if user_edutrain.present?
     invoice_code = user_edutrain.invoice.invoice_note_id
    end
    invoice_code
  end
end
