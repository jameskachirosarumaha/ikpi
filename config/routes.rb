Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, :controllers => { :omniauth_callbacks => "sessions", :sessions => "sessions",  :registrations => "registrations",  :confirmations => "confirmations"}
  
  as :user do 
    match 'signout' => 'sessions#destroy', :as => :signout, :via => Devise.mappings[:user].sign_out_via
    match "/user/invalid_confirmation" => "confirmations#invalid_confirmation", :via => [:get], as: :invalid_confirmation
    post '/sessions' => "sessions#create", :as => :sessions
    post '/sessions/register' => "sessions#register", :as => :registration
    post '/sessions/forgot' => "sessions#forgot", :as => :forgot_password
    get '/sessions/is_login' => "sessions#is_login", :as => :is_login
    get 'user/ask-for-confirmation' => "confirmations#ask_to_confirm", :via => [:get], as: :ask_to_confirm
  end 
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'pages#landing'
  
  resources :edutrains do 
    member do 
      post "register"
    end
    
    collection do 
      get "my_edutrain"
    end
  end
  
  resources :lounges do 
    collection do 
      get "profile"
      get "change_password"
      patch "update_password"
    end
  end
  
  resources :member do
    collection do 
      get "registration"
      get "validate" 
      get "registration/:form_id" => "member#form_type", as: "form_reg_type"
      get "download_user_file/:user_file_id" => "member#download_user_file"
      patch "update_education_history"
      patch "update_work_history"
      patch "update_my_certification"
      patch "update_my_files"
      delete "delete_education_history/:user_education_id" => "member#delete_education_history", as: "delete_member_education_history"
      delete "delete_work_history/:user_work_id" => "member#delete_work_history", as: "delete_member_work_history"
      delete "delete_my_certification/:user_certification_id" => "member#delete_my_certification", as: "delete_member_certification"
      delete "delete_my_files/:user_file_id" => "member#delete_my_file", as: "delete_member_file"
    end
  end
  
  resources :invoices do
    member do 
      get "confirmation"
      post "create_confirmation"
    end
  end

  
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  namespace :admin do
    resources :dashboards
    resources :users do  
      member do 
        get "set_registration_date"
        post "create_registration_date"
      end
    end
    resources :user_registration_forms do 
      member do 
        get "cancel_reg_bills"
      end
    end
    resources :edutrains
    resources :user_registration_files
    resources :invoices do 
      member do 
        get "confirmation"
        post "create_confirmation"
        patch "cancel_confirmation"
      end
      
    end
  end
end
