Recaptcha.configure do |config|

  if Rails.env == "development"
    config.public_key  = '6LeU5f4SAAAAABKjySQAMBUPtQLRi0Bcc8TVRvd_'
    config.private_key = '6LeU5f4SAAAAACJc5ZCnk2Gs5HbafHCx-94BWluO'
  end
  
  if Rails.env == "production"
    config.public_key  = '6LeHMQATAAAAAMQevQw2blGOHulg0HFVuUWup-ZC'
    config.private_key = '6LeHMQATAAAAAMteHjuiXTB6J1QaYhBgBI99k-uz'
  end
  
  # Uncomment the following line if you are using a proxy server:
  # config.proxy = 'http://myproxy.com.au:8080'
end
